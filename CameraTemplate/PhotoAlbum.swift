//
//  PhotoAlbum.swift
//  RawCamera
//
//  Created by Konstantin Medvedenko on 03/30/20.
//  Copyright © 2019 Outofscope. All rights reserved.
//

import Photos
import UIKit

class PhotoAlbum
{
    static let albumName = "Camera Template"
    static let sharedInstance = PhotoAlbum()

    var assetCollection: PHAssetCollection!

    init() {

        func fetchAssetCollectionForAlbum() -> PHAssetCollection!
        {
            let fetchOptions = PHFetchOptions()
            fetchOptions.predicate = NSPredicate(format: "title = %@", PhotoAlbum.albumName)
            let collection = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)

            return collection.firstObject
        }

        if let assetCollection = fetchAssetCollectionForAlbum() {
            self.assetCollection = assetCollection
            return
        }

        PHPhotoLibrary.shared().performChanges({
            PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: PhotoAlbum.albumName)
        }) { success, _ in
            if success {
                self.assetCollection = fetchAssetCollectionForAlbum()
            }
        }
    }

    func save(_ image: UIImage, completion: @escaping () -> ())
    {
        if assetCollection == nil {
            return   // If there was an error upstream, skip the save.
        }

        PHPhotoLibrary.shared().performChanges({
            let assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
            let assetPlaceholder = assetChangeRequest.placeholderForCreatedAsset
            let albumChangeRequest = PHAssetCollectionChangeRequest(for: self.assetCollection)
            if let assetPlaceholder = assetPlaceholder, let albumChangeRequest = albumChangeRequest {
                albumChangeRequest.addAssets([assetPlaceholder] as NSArray)
            }
        }) { (completed, error) in
            completion()
        }
    }
}
