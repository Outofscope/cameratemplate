//
//  ViewController.swift
//  CameraTemplate
//
//  Created by Konstantin Medvedenko on 3/30/20.
//  Copyright © 2020 Outofscope. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

class ViewController: UIViewController {
    
    @IBOutlet var previewView: UIView!
    @IBOutlet var captureButton: UIButton!
    
    private var previewLayer: CALayer?
    private var overlayImageView = UIImageView()

    private enum SessionSetupResult {
        case success
        case notAuthorized
        case configurationFailed
    }
    
    private var setupResult: SessionSetupResult = .success
    private let session = AVCaptureSession()
    
    private let videoDeviceDiscoverySession
        = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera],
                                           mediaType: .video,
                                           position: .back)
    
    private let sessionQueue = DispatchQueue(label: "session queue",
                                             qos: .userInitiated,
                                             attributes: [],
                                             autoreleaseFrequency: .workItem)
    
    private var videoDeviceInput: AVCaptureDeviceInput!
    private let photoOutput = AVCapturePhotoOutput()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupPreviewLayer()
        setupOverlayImageView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        captureButton.layer.cornerRadius = captureButton.frame.size.height / 2
        previewLayer!.frame = previewView.bounds
        overlayImageView.frame = previewView.bounds
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        sessionQueue.async {
            switch self.setupResult {
            case .success:
                self.session.startRunning()
                
            case .notAuthorized:
                DispatchQueue.main.async {
                    self.showPermissionWarning(animated: false)
                }
                break
                
            case .configurationFailed:
                DispatchQueue.main.async {
                    let message = NSLocalizedString("Unable to capture media", comment: "Alert message when something goes wrong during capture session configuration")
                    let alertController = UIAlertController(title: "Raw Experience", message: message, preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        checkCameraPermissions()
        
        sessionQueue.async {
            self.configureSession()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        sessionQueue.async {
            if self.setupResult == .success {
                self.session.stopRunning()
            }
        }
    }
    
    
    // MARK: - Setup
    
    private func setupPreviewLayer()
    {
        previewLayer = AVCaptureVideoPreviewLayer(session: session)
        previewView.layer.addSublayer(previewLayer!)
    }
    
    private func setupOverlayImageView()
    {
        previewView.addSubview(overlayImageView)
        overlayImageView.isHidden = true
    }
    
    
    // MARK: - Capture Session
    
    private func configureSession()
    {
        if setupResult != .success {
            return
        }
        
        let defaultVideoDevice: AVCaptureDevice? = videoDeviceDiscoverySession.devices.first
        
        guard let videoDevice = defaultVideoDevice else {
            print("Could not find any video device")
            setupResult = .configurationFailed
            return
        }
        
        do {
            videoDeviceInput = try AVCaptureDeviceInput(device: videoDevice)
        } catch {
            print("Could not create video device input: \(error)")
            setupResult = .configurationFailed
            return
        }
        
        session.beginConfiguration()
        
        session.sessionPreset = AVCaptureSession.Preset.photo
        
        // Add a video input
        guard session.canAddInput(videoDeviceInput) else {
            print("Could not add video device input to the session")
            setupResult = .configurationFailed
            session.commitConfiguration()
            return
        }
        session.addInput(videoDeviceInput)
        
        // Add photo output
        if session.canAddOutput(photoOutput) {
            session.addOutput(photoOutput)
            
            photoOutput.isHighResolutionCaptureEnabled = true
        } else {
            print("Could not add photo output to the session")
            setupResult = .configurationFailed
            session.commitConfiguration()
            return
        }
        
        session.commitConfiguration()
    }
    
    
    // MARK: - Permissions
    
    private func checkCameraPermissions()
    {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            checkGalleryPermissions()
            break
            
        case .notDetermined:
            sessionQueue.suspend()
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { granted in
                if !granted {
                    self.setupResult = .notAuthorized
                }
                self.sessionQueue.resume()
                
                self.checkGalleryPermissions()
            })
            
        default:
            setupResult = .notAuthorized
        }
    }
    
    private func checkGalleryPermissions()
    {
        switch PHPhotoLibrary.authorizationStatus() {
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({ status in
                self.finalPermissionCheck()
            })
        case .denied:
            finalPermissionCheck()
        default:
            finalPermissionCheck()
            break
        }
    }
    
    private func finalPermissionCheck()
    {
        let cameraAccess = AVCaptureDevice.authorizationStatus(for: .video) == .authorized
        let galleryAccess = PHPhotoLibrary.authorizationStatus() == .authorized

        if !(cameraAccess && galleryAccess) {
            showPermissionWarning(animated: true)
        }
    }

    private func showPermissionWarning(animated: Bool)
    {
        let alert = UIAlertController(title: "Permission warning",
                                      message: "The app needs permissions to Camera and Photo Gallery",
                                      preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        present(alert, animated: animated, completion: nil)
    }
    
    
    // MARK: - Actions
    
    @IBAction func capturePressed() {
        let format = [AVVideoCodecKey: AVVideoCodecType.jpeg]
        let settings = AVCapturePhotoSettings(format: format)
        photoOutput.capturePhoto(with: settings, delegate: self)
        
        overlayImageView.isHidden = false
        overlayImageView.backgroundColor = .white
        
        captureButton.isEnabled = false
    }
}

extension ViewController: AVCapturePhotoCaptureDelegate
{
    func photoOutput(_ output: AVCapturePhotoOutput,
                     didFinishProcessingPhoto photo: AVCapturePhoto,
                     error: Error?) {
        if let cgImage = photo.cgImageRepresentation()?.takeUnretainedValue() {
            let image = UIImage(cgImage: cgImage, scale: 1.0, orientation: .right)
            
            overlayImageView.backgroundColor = .clear
            overlayImageView.image = image
            overlayImageView.isHidden = false
            
            PhotoAlbum.sharedInstance.save(image) {
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.4, animations: {
                        self.overlayImageView.frame.origin.y = self.previewView.frame.size.height
                    }) { _ in
                        self.overlayImageView.image = nil
                        self.overlayImageView.isHidden = true
                        self.overlayImageView.frame.origin.y = 0
                        self.captureButton.isEnabled = true
                    }
                }
            }
        }
    }
}

