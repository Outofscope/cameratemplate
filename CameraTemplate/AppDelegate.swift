//
//  AppDelegate.swift
//  CameraTemplate
//
//  Created by Konstantin Medvedenko on 3/30/20.
//  Copyright © 2020 Outofscope. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
}
